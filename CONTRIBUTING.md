# CONTRIBUTION GUIDE TO ESRH HL7 FRONT-END PROJECT

Contributions are always welcome on this project. If you want to lend us a hand, be sure to follow these little rules for a good merge request.

## Create your own branch

Be sure to create your own branch for your work, based on the up-to-date `develop` branch of this project. Follow the guideline of the [gitflow-avh](https://github.com/petervanderdoes/gitflow-avh) method.

    git checkout develop
    git checkout -b feature/my-new-awesome-feature

## Get your dev test-proof and well documented before the final Merge Request

Before your final push and creating your final merge request, be sure to check the followings:

* all [the unit tests are green](https://static1.squarespace.com/static/518f5d62e4b075248d6a3f90/t/58a830ece6f2e14f1951b58e/1487417607195/?format=1500w);
* you don't have decreased the test coverage of the application;
* be sure to have created and/or updated all [the needed documentation](https://media1.giphy.com/media/2JS3lrvRhyIrC/giphy.gif).

![Satisfied dev](https://media0.giphy.com/media/XreQmk7ETCak0/giphy.gif)
