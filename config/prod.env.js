'use strict'
module.exports = {
  NODE_ENV: '"production"',
  VUE_APP_SERVER_URL: '"http://clotaire-delanchy.fr/api"',
  VUE_APP_WS_URL: '"ws://clotaire-delanchy.fr/api"'
}
